import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.codigoinfo.app',
  appName: 'ionic-proyecto-base',
  webDir: 'dist/ionic-proyecto-base',
  server: {
    androidScheme: 'https'
  }
};

export default config;
