import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { PaisesRoutingModule } from './paises-routing.module';
import { ListaPaisesComponent } from './lista-paises/lista-paises.component';
import { PaisesPipe } from './paises.pipe';



@NgModule({
  declarations: [
    ListaPaisesComponent,
    PaisesPipe
  ],
  imports: [
    CommonModule,
    PaisesRoutingModule,
    IonicModule
  ]
})
export class PaisesModule { }
