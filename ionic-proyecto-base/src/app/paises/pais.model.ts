
export interface IPais {
    name:INames;
    capital?:string[];
    region:string;
    subregion:string;
    population:number;
    continents:string[];
    flags:IFlags;
};

export interface INames {
    common:string;
    official:string;
};

export interface IFlags {
    png:string;
    svg:string;
    alt:string;
}


