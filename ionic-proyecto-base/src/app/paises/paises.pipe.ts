import { Pipe, PipeTransform } from '@angular/core';
import { IPais } from './pais.model';

@Pipe({
  name: 'filtro'
})
export class PaisesPipe implements PipeTransform {

  transform(paisesArray: IPais[], param: string): IPais[] {
    if(param == ''){
      return paisesArray;
    }
    return paisesArray.filter(row=>row.name.common.includes(param) )
  }

}
