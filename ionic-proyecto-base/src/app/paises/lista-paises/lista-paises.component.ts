import { Component, OnInit, inject } from '@angular/core';
import { IPais } from '../pais.model';
import { PaisesService } from '../paises.service';

@Component({
  selector: 'app-lista-paises',
  templateUrl: './lista-paises.component.html'
})
export class ListaPaisesComponent implements OnInit {

  private paisesService = inject(PaisesService);

  paises: IPais[];
  pais_busca:string;

  constructor(){
    this.paises = [];
    this.pais_busca='';
  }

  ngOnInit(): void {
    this.cargarPaises();
  }

  cargarPaises(){
    this.paisesService.obtenerPaises().subscribe(resp=>{
      this.paises = resp as IPais[];
    })
  }

  onBurcar(event:any){
    this.pais_busca = event.detail.value;
    
  }


}
