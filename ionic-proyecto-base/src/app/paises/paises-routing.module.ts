import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaPaisesComponent } from './lista-paises/lista-paises.component';

const routes: Routes = [
  {
    path:'lista-paises',
    component:ListaPaisesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaisesRoutingModule { }
