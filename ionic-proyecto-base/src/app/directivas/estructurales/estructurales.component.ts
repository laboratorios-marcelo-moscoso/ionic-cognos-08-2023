import { Component, OnInit } from '@angular/core';
import { IAlumno } from '../alumno.model';

@Component({
  selector: 'app-estructurales',
  templateUrl: './estructurales.component.html',
  styleUrls: ['./estructurales.component.css']
})
export class EstructuralesComponent implements OnInit {

  alumno: IAlumno;
  alumnos: IAlumno[];
  cursos: string[];
  esnodejs : boolean = false;

  constructor() {
    this.alumno = { ci: 0, apellido: '', curso: '', nombre: '' };
    this.alumnos = [];
    this.cursos = [];
  }

  ngOnInit(): void {
    this.cargarCursos();
    this.cargarAlumnos();
  }

  cargarCursos() {
    this.cursos = ['Angular', 'Nodejs', 'ReactJs', 'VueJs', 'MongoDB', 'Flutter'];
  }

  cargarAlumnos() {
    this.alumnos = [
      { ci: 123456, nombre: "Ana", apellido: "Gomez", curso: "Angular" },
      { ci: 223344, nombre: "Jorge", apellido: "Chavez", curso: "Angular" }, 
      { ci: 111111, nombre: "Marco", apellido: "Lopez", curso: "Nodejs" },
      { ci: 654321, nombre: "Jose", apellido: "Monje", curso: "Nodejs" }
    ]
  }


}
