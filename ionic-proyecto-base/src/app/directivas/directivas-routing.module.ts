import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AtributosComponent } from './atributos/atributos.component';
import { ComponentesComponent } from './componentes/componentes.component';
import { EstructuralesComponent } from './estructurales/estructurales.component';

const routes: Routes = [
  {
    path:'atributos',
    component:AtributosComponent
  },
  {
    path:'componentes',
    component:ComponentesComponent
  },
  {
    path:'estructurales',
    component:EstructuralesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DirectivasRoutingModule { }
