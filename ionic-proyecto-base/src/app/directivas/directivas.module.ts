import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { DirectivasRoutingModule } from './directivas-routing.module';
import { ComponentesComponent } from './componentes/componentes.component';
import { EstructuralesComponent } from './estructurales/estructurales.component';
import { AtributosComponent } from './atributos/atributos.component';
import { RealceDirective } from './realce.directive';


@NgModule({
  declarations: [
    ComponentesComponent,
    EstructuralesComponent,
    AtributosComponent,
    RealceDirective
  ],
  imports: [
    CommonModule,
    DirectivasRoutingModule,
    IonicModule
  ]
})
export class DirectivasModule { }
