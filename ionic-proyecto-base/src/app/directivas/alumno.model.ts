export interface IAlumno {
    ci: number, 
    nombre: string, 
    apellido: string, 
    curso: string
}
