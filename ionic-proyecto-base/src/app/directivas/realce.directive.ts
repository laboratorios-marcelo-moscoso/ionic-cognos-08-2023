import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appRealce]'
})
export class RealceDirective {

  @Input('appRealce') colorSet: string=''; 
  
  constructor(private el: ElementRef) { }

  @HostListener('mouseenter') onMouseEnter() { 
    this.realce('yellow');
  }

  @HostListener('mouseleave') onMouseLeave() { 
    this.realce('blue');
  }

  private realce(color: string) { this.el.nativeElement.style.backgroundColor = color;
  }

}
