import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IProduct,IProductList } from './product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  URL_API: string = 'https://dummyjson.com/products';

  constructor(private http: HttpClient) { }

  getProducts(){
    return this.http.get(this.URL_API);
  }

}
