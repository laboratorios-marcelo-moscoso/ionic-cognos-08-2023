import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductListComponent } from './product-list/product-list.component';
import { CartListComponent } from './cart-list/cart-list.component';

const routes: Routes = [
  {
    path:'product-list',
    component:ProductListComponent
  },
  {
    path:'cart-list',
    component:CartListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReactividadRoutingModule { }
