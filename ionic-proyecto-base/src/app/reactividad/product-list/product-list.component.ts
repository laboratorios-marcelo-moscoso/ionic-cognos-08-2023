import { Component, OnInit, inject } from '@angular/core';
import { ProductService } from '../product.service';
import { IProductList,IProduct } from '../product.model';

import { StoreService } from '../store.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  private productService = inject(ProductService);
  private storeService = inject(StoreService);

  productList : IProductList;
  myCart$ = this.storeService.myCart$;

  constructor(){
    this.productList = { products:[],total:0,skip:0,limit:0}
  }

  ngOnInit(): void {
    this.loadProducts();
  }

  loadProducts(){
    this.productService.getProducts().subscribe(resp=>{
      this.productList = resp as IProductList
    })
  }

  onAddToCart(product:IProduct){
    this.storeService.addProductToCart(product)
  }


}
