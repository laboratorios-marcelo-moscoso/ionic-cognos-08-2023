import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { ReactividadRoutingModule } from './reactividad-routing.module';
import { ProductListComponent } from './product-list/product-list.component';
import { CartListComponent } from './cart-list/cart-list.component';


@NgModule({
  declarations: [
    ProductListComponent,
    CartListComponent
  ],
  imports: [
    CommonModule,
    ReactividadRoutingModule,
    IonicModule
  ]
})
export class ReactividadModule { }
