import { Component, inject } from '@angular/core';
import { IProduct } from '../product.model';
import { StoreService } from '../store.service';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.css']
})
export class CartListComponent {

  private storeService = inject(StoreService);
  myCart$ = this.storeService.myCart$;

}
