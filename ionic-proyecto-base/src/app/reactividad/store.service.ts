import { Injectable } from '@angular/core';
import { IProduct } from './product.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private myProduct : IProduct[];
  private myCart = new BehaviorSubject<IProduct[]>([]);
  myCart$ = this.myCart.asObservable();

  constructor() {
    this.myProduct = [];
  }

  addProductToCart(product:IProduct){
    this.myProduct.push(product);
    this.myCart.next(this.myProduct);
  }

  removeProductToCart(){
    
  }

}
