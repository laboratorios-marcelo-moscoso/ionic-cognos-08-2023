export interface IAlumno {
    _id:string;
    nombre_completo:string;
    doc_identidad:number;
    imagen:string;
}
