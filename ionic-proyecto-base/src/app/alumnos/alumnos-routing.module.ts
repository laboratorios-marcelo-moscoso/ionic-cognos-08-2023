import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlumnoFrmComponent } from './alumno-frm/alumno-frm.component';
import { AlumnosLstComponent } from './alumnos-lst/alumnos-lst.component';

const routes: Routes = [
  {
    path:'alumno-frm/:id',
    component:AlumnoFrmComponent
  },
  {
    path:'alumnos-lst',
    component:AlumnosLstComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlumnosRoutingModule { }
