import { Component, inject , OnInit} from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { AlumnoService } from '../alumno.service';
import { PhothosService} from '../../services/photos.service';
import { IAlumno } from '../alumno.modelo';


@Component({
  selector: 'app-alumno-frm',
  templateUrl: './alumno-frm.component.html',
  styleUrls: ['./alumno-frm.component.css']
})
export class AlumnoFrmComponent implements OnInit {

  private alumnosService = inject(AlumnoService);
  private router = inject(Router);
  private actiRoute = inject(ActivatedRoute );
  private fotoService = inject(PhothosService);

  _alumno: IAlumno;
  isToastOpen = false;
  messageToast = '';
  titulo ='nuevo';

  photos: string[] = [];

  constructor(){
    this._alumno = {
      _id : '',
      nombre_completo : '',
      doc_identidad : 0,
      imagen : ''
    };
    this.photos = this.fotoService.photos;
  }

  ngOnInit(): void {
    const {id} = this.actiRoute.snapshot.params;
    console.log(id);
    if(id == '1'){

    }else{
      this.alumnosService.getAlumno(id).subscribe(resp=>{
        const response = resp as {status:string,message:string,data:IAlumno[]};
        if(response.status == 'OK'){
          this._alumno = response.data[0];
          this.titulo = this._alumno._id;
        }else{
          this.messageToast = `ERROR:${response.message}`;
          this.setOpen(true);
        };
      })
    }
  }

  onSubmit(form:NgForm ){

    if(form.valid){

      if(this._alumno._id){
        this.alumnosService.updateAlumno(this._alumno).subscribe(resp=>{
          const response = resp as {status:string,message:string,data:{}};
          if(response.status == 'OK'){
            this.setOpen(true);
            this.messageToast = 'Se actualizo con exito';

          }else{
            this.messageToast = `ERROR:${response.message}`;
            this.setOpen(true);
          };
        })
      }else{
        this.alumnosService.addAlumno(this._alumno).subscribe(resp=>{
          const response = resp as {status:string,message:string,data:{}};
          if(response.status == 'OK'){
            this.setOpen(true);
            this.messageToast = 'Se registro con exito';
  
            this.router.navigateByUrl("/alumnos/alumnos-lst");
          }else{
            this.messageToast = `ERROR:${response.message}`;
            this.setOpen(true);
          };
          
        })
      }



    }
    
  }

  setOpen(isOpen:boolean){
    this.isToastOpen = isOpen;
  }

  roleMessage = '';

  public alertButtons = [
    {
      text: 'Cancelar',
      role: 'cancel',
      handler: () => {
        
      },
    },
    {
      text: 'Eliminar',
      role: 'confirm',
      handler: () => {
        
        this.alumnosService.removealumno(this._alumno._id).subscribe(resp=>{
          const response = resp as {status:string,message:string,data:{}};
          if(response.status == 'OK'){
            this.setOpen(true);
            this.messageToast = 'Registro';
  
            this.router.navigateByUrl("/alumnos/alumnos-lst");
          }else{
            this.messageToast = `ERROR:${response.message}`;
            this.setOpen(true);
          };
        })

      },
    },
  ];

  setResult(ev:any) {
    this.roleMessage = `Dismissed with role: ${ev.detail.role}`;
  };



  async tomarFoto(){
    await this.fotoService.addNewPhotos();
  }


}
