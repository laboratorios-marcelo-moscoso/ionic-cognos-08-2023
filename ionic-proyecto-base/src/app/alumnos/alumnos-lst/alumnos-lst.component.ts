import { Component, OnInit, inject } from '@angular/core';
import { AlumnoService } from '../alumno.service';
import { IAlumno } from '../alumno.modelo';

@Component({
  selector: 'app-alumnos-lst',
  templateUrl: './alumnos-lst.component.html',
  styleUrls: ['./alumnos-lst.component.css']
})
export class AlumnosLstComponent implements OnInit {

  private alumnosService = inject(AlumnoService);
  _alumnosLst:IAlumno[];


  constructor(){
    this._alumnosLst = [];
  }

  ngOnInit(): void {
    this.loadAlumnos();
  }

  loadAlumnos(){
    this.alumnosService.getAlumnos().subscribe(resp=>{
      const response = resp as {status:string,message:string,data:IAlumno[]};
      if(response.status =='OK'){
        this._alumnosLst = response.data;
      }
    })
  }

  ionViewWillEnter(){
    this.loadAlumnos();
  }



}


