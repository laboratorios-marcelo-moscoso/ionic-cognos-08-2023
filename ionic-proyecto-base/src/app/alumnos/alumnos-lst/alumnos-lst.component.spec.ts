import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlumnosLstComponent } from './alumnos-lst.component';

describe('AlumnosLstComponent', () => {
  let component: AlumnosLstComponent;
  let fixture: ComponentFixture<AlumnosLstComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AlumnosLstComponent]
    });
    fixture = TestBed.createComponent(AlumnosLstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
