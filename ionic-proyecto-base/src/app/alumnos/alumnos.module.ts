import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms'

import { AlumnosRoutingModule } from './alumnos-routing.module';
import { AlumnoFrmComponent } from './alumno-frm/alumno-frm.component';
import { AlumnosLstComponent } from './alumnos-lst/alumnos-lst.component';


@NgModule({
  declarations: [
    AlumnoFrmComponent,
    AlumnosLstComponent
  ],
  imports: [
    CommonModule,
    AlumnosRoutingModule,
    IonicModule,
    FormsModule
  ]
})
export class AlumnosModule { }
