import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IAlumno } from './alumno.modelo';

@Injectable({
  providedIn: 'root'
})
export class AlumnoService {

  URL_API: string = 'https://codigoinfo.xyz/api/alumnos';

  constructor(private http: HttpClient) {}

  getAlumnos(){
    return this.http.get(this.URL_API);
  }

  getAlumno(_id:string){
    return this.http.get(`${this.URL_API}/${_id}`);
  }

  addAlumno(alumno:IAlumno){
    return this.http.post(this.URL_API,alumno);
  }

  updateAlumno(alumno:IAlumno){
    return this.http.put(`${this.URL_API}/${alumno._id}`,alumno)
  }

  removealumno(_id:string){
    return this.http.delete(`${this.URL_API}/${_id}`);
  }

}
