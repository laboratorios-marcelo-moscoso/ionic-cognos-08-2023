import { Component } from '@angular/core';
import { PhothosService } from '../../services/photos.service';


@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html'
})
export class GalleryComponent {

  photos: string[] = [];

  constructor(
    private photosServices: PhothosService
  ){
    this.photos = this.photosServices.photos;
  }

  async takePhotos(){
    await this.photosServices.addNewPhotos();
  }


}
