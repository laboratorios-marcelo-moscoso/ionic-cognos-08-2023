import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { ReactiveFormsModule } from '@angular/forms';

import { FormulariosReactivosRoutingModule } from './formularios-reactivos-routing.module';
import { RegistroAlumnoComponent } from './registro-alumno/registro-alumno.component';


@NgModule({
  declarations: [
    RegistroAlumnoComponent
  ],
  imports: [
    CommonModule,
    FormulariosReactivosRoutingModule,
    IonicModule,
    ReactiveFormsModule
  ]
})
export class FormulariosReactivosModule { }
