import { Component, inject } from '@angular/core';
import { FormBuilder,Validators } from '@angular/forms';

@Component({
  selector: 'app-registro-alumno',
  templateUrl: './registro-alumno.component.html',
  styleUrls: ['./registro-alumno.component.css']
})
export class RegistroAlumnoComponent {

  private fb = inject(FormBuilder);

  alumnoFrm = this.fb.group({
    nombre_completo : ['',[Validators.required]],
    doc_identidad : ['',[Validators.required,Validators.pattern('^[0-9]{3,}')]],
    email:['',[Validators.required,Validators.email]],
    usuario:['',[Validators.required,Validators.pattern('^[a-zA-Z0-9_-]{3,16}$')]],
    password:['',[Validators.required,Validators.pattern('^[a-zA-Z0-9_-].*(?=.{8,})(?=..*[0-9])(?=.*[!@#$%^&+=]).*$')]]
  });

  get f(){
    return this.alumnoFrm.controls
  }

  onSubmit(){
    if(this.alumnoFrm.valid){
      const alumno = this.alumnoFrm.getRawValue();
      console.log(alumno);
    }

  }


}
