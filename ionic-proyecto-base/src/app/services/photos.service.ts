import { Injectable } from '@angular/core';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';

@Injectable({
  providedIn: 'root'
})
export class PhothosService {

  photos: string[] = [];
  imagenUrl:string = '';

  constructor() { }

  async addNewPhotos(){
    const photos_ = await Camera.getPhoto(
      {
        resultType: CameraResultType.Uri,
        source: CameraSource.Camera,
        quality:100
      }
    );

    if(photos_.webPath){
      this.photos.unshift(photos_.webPath)
    }

  }

  async getFoto(){
    const photos_ = await Camera.getPhoto(
      {
        resultType: CameraResultType.Uri,
        source: CameraSource.Camera,
        quality:100
      }
    );

    if(photos_.webPath){
      this.imagenUrl = photos_.webPath
    }
    
  }

}
