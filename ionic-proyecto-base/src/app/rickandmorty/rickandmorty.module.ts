import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { RickandmortyRoutingModule } from './rickandmorty-routing.module';
import { ListaComponent } from './lista/lista.component';


@NgModule({
  declarations: [
    ListaComponent
  ],
  imports: [
    CommonModule,
    RickandmortyRoutingModule,
    IonicModule
  ]
})
export class RickandmortyModule { }
