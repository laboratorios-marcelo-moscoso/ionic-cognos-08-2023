import { Component, OnInit, inject } from '@angular/core';

import { IRickandmorty,ICharacter } from '../rickandmorty.model';
import { RickandmortyService } from '../rickandmorty.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

    private rickandmortyService = inject(RickandmortyService);
    characters : ICharacter[];

    constructor(){
      this.characters = [];
    }

    ngOnInit(): void {
      this.loadCharacters();
    }

    loadCharacters(){
      this.rickandmortyService.obtenerPersonajes().subscribe(resp=>{
        const response = resp as IRickandmorty;

        this.characters = response.results;

      })
    }

}
