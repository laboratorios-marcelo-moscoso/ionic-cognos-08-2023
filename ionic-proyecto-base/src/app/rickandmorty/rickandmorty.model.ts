export interface IRickandmorty {
    info:IInfo;
    results : ICharacter[];
}

interface IInfo {
    count:number;
    pages: number;
    next:string;
    prev:null;
}

export interface ICharacter {
    id: number;
    name : string;
    image : string;
}
