import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlumnoModel } from '../alumno.model';

@Component({
  selector: 'app-registro-alumno',
  templateUrl: './registro-alumno.component.html',
  styleUrls: ['./registro-alumno.component.css']
})
export class RegistroAlumnoComponent {

  alumno : AlumnoModel;

  constructor(){
    this.alumno = { 
      nombre_completo : '',
      doc_identidad : 0,
      email : '',
      usuario : '',
      password : ''
    };
  }

  onSubmit(form:NgForm ){

    if(form.valid){
      console.log(this.alumno)
    }
    
  }
  

}
