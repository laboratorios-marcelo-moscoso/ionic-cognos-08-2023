import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms'

import { TemplateFormsRoutingModule } from './template-forms-routing.module';
import { RegistroAlumnoComponent } from './registro-alumno/registro-alumno.component';


@NgModule({
  declarations: [
    RegistroAlumnoComponent
  ],
  imports: [
    CommonModule,
    TemplateFormsRoutingModule,
    IonicModule,
    FormsModule
  ]
})
export class TemplateFormsModule { }
