
import { createReducer,on } from '@ngrx/store';
import { incrementar,decrementar,inicializar }from './contador.actions';

export const initialState = 0;

export const contadorReducer =  createReducer(
    initialState,
    on(incrementar,(state)=> state + 1),
    on(decrementar,(state)=> state - 1),
    on(inicializar,(state)=> 0)
);