import { createAction } from '@ngrx/store';

export const incrementar = createAction('[Contador Component] Incrementar');
export const decrementar = createAction('[Contador Component] Decrementar');
export const inicializar = createAction('[Contador Component] Inicializar');
