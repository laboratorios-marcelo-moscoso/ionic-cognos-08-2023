import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { incrementar,decrementar,inicializar } from '../contador.actions';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent {

  contador$ : Observable<number>;

  constructor(private store: Store<{ contador: number}> ){
    this.contador$ = store.select('contador');
  }

  onIncrementar(){
    this.store.dispatch(incrementar());
  }

  onDecrementar(){
    this.store.dispatch(decrementar());
  }

  onInicializar(){
    this.store.dispatch(inicializar());
  }

}
