import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { StoreModule } from '@ngrx/store';

import { ReduxRoutingModule } from './redux-routing.module';
import { ContadorComponent } from './contador/contador.component';
import { contadorReducer } from './contador.reducer';


@NgModule({
  declarations: [
    ContadorComponent
  ],
  imports: [
    CommonModule,
    ReduxRoutingModule,
    IonicModule,
    StoreModule.forRoot({contador:contadorReducer})
  ]
})
export class ReduxModule { }
