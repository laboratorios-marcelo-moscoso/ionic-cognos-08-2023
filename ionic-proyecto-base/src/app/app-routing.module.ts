import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { DetailComponent } from './pages/detail/detail.component';
import { AboutComponent } from './pages/about/about.component';
import { GalleryComponent } from './pages/gallery/gallery.component';
import { TabsComponent } from './pages/tabs/tabs.component';

const routes: Routes = [
  {
    path:'',
    redirectTo:'home',
    pathMatch: 'full'
  },
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'about',
    component:AboutComponent
  },
  {
    path:'detail',
    component:DetailComponent
  },
  {
    path: 'gallery',
    component:GalleryComponent
  },
  {
    path: 'template-forms',
    loadChildren : ()=> import('./template-forms/template-forms.module').then(m=>m.TemplateFormsModule)
  },
  {
    path:'reactive-forms',
    loadChildren: ()=> import('./formularios-reactivos/formularios-reactivos.module').then(m=>m.FormulariosReactivosModule)
  },
  {
    path:'paises',
    loadChildren: ()=> import('./paises/paises.module').then(m=>m.PaisesModule)
  },
  {
    path:'alumnos',
    loadChildren: ()=> import('./alumnos/alumnos.module').then(m=>m.AlumnosModule)
  },
  {
    path:'redux',
    loadChildren:()=> import('./redux/redux.module').then(m=>m.ReduxModule)
  },
  {
    path:'reactividad',
    loadChildren:()=> import('./reactividad/reactividad.module').then(m=>m.ReactividadModule)
  },
  {
    path:'rickandmorty',
    loadChildren: ()=>import('./rickandmorty/rickandmorty.module').then(m=>m.RickandmortyModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
